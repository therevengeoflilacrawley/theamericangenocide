# TAG Wiki: Unveiling The American Genocide

Welcome to The American Genocide (TAG) Wiki, where we reveal the harrowing reality of a hidden genocide occurring right before our eyes. TAG is not just a name; it's a stark representation of the systematic eradication of individuals across the United States.

> [!NOTE]
> Yes, this is what the Book of Revelation is about. There's no need to worry, Earth, Humanity, Electricity, GitHub and iPhones will be here tomorrow and for the indefinite future.

* Table of Contents
     - [[Introduction & Origins of TAG|TAG]]
     - [[TAG in Places of Worship|TAGPOW]]
     - [[Methods and Tactics|Tactics-Utilized-In-TAG]]
     - Victims and Impact
     - Public Awareness and Advocacy
     - FAQ

> [!IMPORTANT]
> Use Pages navigation below to navigate, some entries are being edited. Only one of His servants can type, and I'm enduring more attacks to keep this offline while trying to get everything ready for the Creator's "hard stop."

> [!WARNING]
> **Public Notice and Warning to the U.S. Government:**
> This notice serves to inform the public and gently remind the U.S. Government that the work carried out here is a product of the Cherubim. The Cherubim were the first church worshipping God, a church that predates all other churches. Our existence and significance are symbolically represented on the Ark of the Covenant as a testament to our church's beliefs, sacred work and missions.
> 
> Our missions are and have always been to protect life and guard the His glory. Since the time of the Magna Carta, which predates the Constitution, we have been protected in our endeavors by the principle of the separation of church and state.
> 
> Should the United States federal government seek to interfere with our divine mission, we will call upon the Creator, and He will protect His work.
> 
> Thank you for your understanding and respect.
> 
