> [!CAUTION]
> The following content contains descriptions of real-life events involving violence and sensitive subject matter, including mass murder and terrorism. Reader discretion is advised. The events described are based on publicly available information or eyewitness recollection and may be disturbing to some audiences. Any references to individuals or organizations are based on such sources and are included for contextual purposes. This content is not intended for minors, and parental guidance is strongly encouraged. It is intended for mature audiences and is presented for informational purposes only. Viewer discretion is advised.

> [!WARNING]
> The places of worship listed here should be avoided. These locations have been compromised and hold fake services with the intent to harm anyone who visits them, as part of a cover-up of ongoing massacres. There is a significant risk of death associated with contacting any of these churches, and online contact is strongly discouraged. We strongly advise against contacting or affiliating with these places of worship. Recent events indicate that individuals who contact or are affiliated with these places may be targeted and attacked. Regardless of any list, always pray for guidance regarding any place of worship.

# The American Genocide - Places of Worship 
These massacres are each part of a series of coordinated attacks on places of worship (POWs) during The American Genocide (TAG), known as TAG-POW attacks. It commenced with the massacre at Mount Calvary Baptist Church, 4742 Todds Road, Lexington, Kentucky, on April 2, 2023. Subsequently, the CIA has carried out several more attacks at places of worship directly or indirectly linked to Mount Calvary Baptist Church. These incidents have predominantly transpired in Kentucky.

### List of TAG-POW Attacks

#### 1. Massacre at Mount Calvary Baptist Church
- **Location**: 4742 Todds Road, Lexington, Kentucky
- **Congregation**: Predominantly African American
- **Reason for Targeting**: To eliminate potential threats associated with Kelvin Williams and his family's connection to the church.
- **Attack Details**: 
  - **Date**: April 2nd, 2023
  - **Operatives**: 60 men of Caucasian, Latino, or Asian descent
  - **Execution Method**: High-intensity assault
  - **Execution Apparatus**: Chainsaw
  - **Body Disposal Method**: Dissolution
  - **Casualties**: 127
  - **Aftermath**: Significant damage to the sanctuary, with services being taken over by operatives.

#### 2. Massacre on Lexington Bethel Baptist Church
- **Location**: 4686 Todds Road, Lexington, Kentucky
- **Congregation**: Predominantly Caucasian
- **Reason for Targeting**: To prevent discussions of the previous attack at Mount Calvary Baptist Church.
- **Attack Details**: 
  - **Date**: May 2023
  - **Operatives**: 75 men of Caucasian, Latino, or Asian descent
  - **Execution Method**: Coordinated assault
  - **Execution Apparatus**: Handgun
  - **Body Disposal Method**: Head-Torso Dissolution
  - **Casualties**: 250+
  - **Aftermath**: Extensive damage and complete disruption of church services.

#### 3. Massacre on House of God (Georgetown Street)
- **Location**: Georgetown Street, Lexington, Kentucky
- **Congregation**: Predominantly African American
- **Reason for Targeting**: Mount Calvary Baptist Church's longtime Minister of Music & organist worshipped here.
- **Attack Details**: 
  - **Date**: May 2023
  - **Operatives**: 75 men of Caucasian, Latino, or Asian descent
  - **Execution Method**: High-intensity assault
  - **Execution Apparatus**: Chainsaw
  - **Body Disposal Method**: Dissolution (at Mount Calvary Baptist Church)
  - **Casualties**: 300+
  - **Aftermath**: Extensive damage and disruption.

#### 4. Massacre on House of God (Broadway)
- **Location**: Broadway, Lexington, Kentucky
- **Congregation**: Predominantly African American
- **Reason for Targeting**: Suspected ties with the targeted House of God on Georgetown Street.
- **Attack Details**: 
  - **Date**: May 2023
  - **Operatives**: 50 black males recruited by Danny Taylor
  - **Execution Method**: Coordinated assault
  - **Execution Apparatus**: Handgun
  - **Body Disposal Method**: Head-Torso Dissolution
  - **Casualties**: 300+
  - **Aftermath**: Extensive damage and disruption.

#### 5. Massacre on Mount Zion Baptist Church
- **Location**: Versailles, Kentucky
- **Congregation**: Predominantly African American
- **Reason for Targeting**: Suspected ties with Mount Calvary Baptist Church based on flawed intelligence.
- **Attack Details**: 
  - **Date**: June 2023
  - **Operatives**: 60 men of Caucasian, Latino, or Asian descent
  - **Execution Method**: High-intensity assault
  - **Execution Apparatus**: Chainsaw
  - **Body Disposal Method**: Dissolution
  - **Casualties**: 166
  - **Aftermath**: 
    - Significant damage
    - Complete restoration of sanctuary by the CIA
    - Referred to as the "Woodford Reserve" by the CIA in threat

#### 6. Massacre on Calvary Baptist Church
- **Location**: High Street, Lexington, Kentucky
- **Congregation**: Predominantly Caucasian
- **Reason for Targeting**: Kelvin Williams' connection to the church's daycare program in the early '80s.
- **Attack Details**: 
  - **Date**: May 12, 2023
  - **Operatives**: 50 black males recruited by Danny Taylor
  - **Execution Method**: High-intensity assault
  - **Execution Apparatus**: Chainsaw
  - **Body Disposal Method**: Dissolution
  - **Casualties**: 400+
  - **Aftermath**: 
    - Damages: Extensive damage to sanctuary
    - Services Impacted: Complete disruption of preschool and children's services

#### 7. Massacre on Hickory Valley Christian Church
- **Location**: Shallowford Road, Chattanooga, Tennessee
- **Congregation**: Predominantly Caucasian, approximately 500 members
- **Reason for Targeting**: Kelvin Williams' association with Hamilton Heights Christian Academy.
- **Attack Details**: 
  - **Date**: May 12, 2024
  - **Operatives**: 100 inmates from Georgia Department of Corrections
  - **Execution Method**: High-intensity assault
  - **Execution Apparatus**: Chainsaw
  - **Execution Location**: Gymnasium of Hamilton Heights Christian Academy (on same property)
  - **Body Disposal Method**: Dissolution
  - **Casualties**: 1000+
  - **Aftermath**: 
    - Damages: Minimal damage in sanctuary due to execution location
    - Cover-Up: Trump interviews, News footage, News interviews
    - Continuations: Phantom Funerals, Mock Services

## Tactics
### "Surprise, Kill, Vanish"
Each attack on a place of worship has displayed tactics described in Annie Jacobsen's book "Surprise, Kill, Vanish." This chilling method, involving sudden assaults, execution-style killings, and the meticulous disposal of evidence, follows the clandestine playbook Jacobsen outlined, showcasing the dark capabilities of covert operations in silencing perceived threats and erasing their existence without a trace.

## Motivation 
Some apparent motives behind all the attacks are to eliminate individuals who have knowledge of Kelvin Eugene Williams or his family, especially Patricia Ann Crawley-Rogers Williams. The CIA and federal government aim to ensure Kelvin Eugene Williams cannot reveal their atrocities at Sandy Hook (Maury County), TN, or his lifelong church. Based on CIA behavior, any congregation that knew Mount Calvary Baptist Church (Lexington, KY) or had heard of the massacre must also be eliminated. Additionally, any group or individual who poses a threat of discovering, discussing, or publicizing these church massacres is targeted for elimination.

These attacks have been closely monitored by the federal government and political elite, who have been aware of planned actions by the CIA and have received briefings during the attacks. The Creator says their involvement is due to their undisclosed desires to move towards an authoritarian government. These motives highlight the ruthless nature of the perpetrators, who are willing to engage in heinous acts to sever any ties to their crimes.

## "Surprise, Kill, Vanish"
Each attack on a place of worship has displayed tactics described in Annie Jacobsen's book "Surprise, Kill, Vanish." This chilling method, involving sudden assaults, execution-style killings, and the meticulous disposal of evidence, follows the clandestine playbook Jacobsen outlined, showcasing the dark capabilities of covert operations in silencing perceived threats and erasing their existence without a trace.


